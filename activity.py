# 1. Create an abstract class called Animal that has the following abstract methods:
# eat(food)
# make_sound()

from abc import ABC

class Animal(ABC):
    def eat_food(self, food):
        pass
    def make_sound(self):
        pass


class Dog(Animal):
    def __init__(self, name, breed, age):
        super().__init__()
        self.name = name
        self.breed = breed
        self.age = age
    def eat_food(self, food):
        print(f'Eaten {food}')
    def make_sound(self):
        print(f'Bark! Woof! Arf!')
    def get_name(self):
        f'{self.name}'
    def set_name(self, name):
        self._name = name
    def get_breed(self):
       f'{self.breed}'
    def set_breed(self, breed):
        self._breed = breed
    def get_age(self):
       f'{self.age}'
    def set_age(self, age):
        self._age = age
    def call(self):
        print(f'Here {self.name}!')
    
class Cat(Animal):
    def __init__(self, name, breed, age):
        super().__init__()
        self.name = name
        self.breed = breed
        self.age = age
    def eat_food(self, food):
        print(f'Serve me {food}')
    def make_sound(self):
        print(f'Miaow! Nyaw! Nyaaaaaaaaaaaaaaa!')
    def get_name(self):
        f'{self.name}'
    def set_name(self, name):
        self._name = name
    def get_breed(self):
       f'{self.breed}'
    def set_breed(self, breed):
        self._breed = breed
    def get_age(self):
       f'{self.age}'
    def set_age(self, age):
        self._age = age
    def call(self):
        print(f'{self.name}, Come on! ')   


dog1 = Dog('Isis', 'Dalmatian', 15)
dog1.eat_food('Steak')
dog1.make_sound()
dog1.call()


cat1 = Cat('Puss', 'Persian', 4)
cat1.eat_food('Tuna')
cat1.make_sound()
cat1.call()
    







# 2Create two classes that implements the Animal class called Cat and Dog with each of the following properties and methods:
# Properties:
# Name
# Breed
# Age
# Methods:
# Getters
# Setters
# Implementation of abstract methods
# call()

